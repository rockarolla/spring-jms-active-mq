package mx.raul.activemq.queueproducerconsumer;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.RedeliveryPolicy;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.activemq.ActiveMQProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.jms.support.destination.DynamicDestinationResolver;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.jms.ConnectionFactory;

@Configuration
@EnableScheduling
@EnableJms
@SpringBootApplication
public class QueueProducerConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(QueueProducerConsumerApplication.class, args);
	}

	@Bean
	@ConfigurationProperties(prefix = "spring.activemq")
	public ActiveMQProperties myActiveMQProperties() {
		return new ActiveMQProperties();
	}

	@Bean
	public ConnectionFactory activeMqConnectionFactory(
			@Qualifier("myActiveMQProperties") ActiveMQProperties properties) {
		ActiveMQConnectionFactory activeMQConnectionFactory =
				new ActiveMQConnectionFactory(properties.getBrokerUrl());
		activeMQConnectionFactory.getPrefetchPolicy().setQueuePrefetch(1);
		RedeliveryPolicy redeliveryPolicy = activeMQConnectionFactory.getRedeliveryPolicy();
		redeliveryPolicy.setUseExponentialBackOff(true);
		redeliveryPolicy.setMaximumRedeliveries(4);
		return activeMQConnectionFactory;
	}

	@Bean
	public ConnectionFactory cachingConnectionFactory(
			@Qualifier("activeMqConnectionFactory") ConnectionFactory activeMqConnectionFactory) {
		CachingConnectionFactory cachingConnectionFactory =
				new CachingConnectionFactory(activeMqConnectionFactory);
		cachingConnectionFactory.setCacheConsumers(true);
		cachingConnectionFactory.setCacheProducers(true);
		cachingConnectionFactory.setReconnectOnException(true);
		return cachingConnectionFactory;
	}

	@Bean
	public MessageConverter jacksonMessageConverter() {
		MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
		converter.setTargetType(MessageType.TEXT);
		converter.setTypeIdPropertyName("_messageType");
		return converter;
	}

	@Bean
	public JmsTemplate queueJmsTemplate(
			@Qualifier("cachingConnectionFactory") ConnectionFactory connectionFactory) {
		JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory);
		jmsTemplate.setDestinationResolver(new DynamicDestinationResolver());
		jmsTemplate.setMessageConverter(jacksonMessageConverter());
		jmsTemplate.setPriority(4);
		jmsTemplate.setTimeToLive(30000L);
		jmsTemplate.setExplicitQosEnabled(true);

		jmsTemplate.setDefaultDestinationName("raul-messages-queue");
		return jmsTemplate;
	}

	/////////////////////////////////////////////////////////////////////////////////////

	@Bean(name = "QueueListenerContainerFactory")
	public JmsListenerContainerFactory jmsListenerContainerFactory(
			@Qualifier("activeMqConnectionFactory") ConnectionFactory connectionFactory
	) {
		DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
		factory.setConnectionFactory(connectionFactory);
		factory.setDestinationResolver(new DynamicDestinationResolver());
		factory.setConcurrency("3-10");
		factory.setClientId("raul-id");
		factory.setMessageConverter(jacksonMessageConverter());
		return factory;
	}

}
