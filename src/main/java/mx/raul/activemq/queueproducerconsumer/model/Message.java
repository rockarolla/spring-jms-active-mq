package mx.raul.activemq.queueproducerconsumer.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Message {

  private String message;
}
