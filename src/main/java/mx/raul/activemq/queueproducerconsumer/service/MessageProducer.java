package mx.raul.activemq.queueproducerconsumer.service;

import mx.raul.activemq.queueproducerconsumer.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class MessageProducer {

  private JmsTemplate jmsTemplate;

  private int messageId = 0;

  @Autowired
  public MessageProducer(JmsTemplate jmsTemplate) {
    this.jmsTemplate = jmsTemplate;
  }

  @Scheduled(fixedDelay = 33000)
  public void produceMessage() {
    Message message = new Message();
    message.setMessage("This is the message [[" + (++messageId) + "]] content");

    this.jmsTemplate.convertAndSend("raul-messages-queue", message);
    System.out.println("SENT: " + message.getMessage());
  }
}
