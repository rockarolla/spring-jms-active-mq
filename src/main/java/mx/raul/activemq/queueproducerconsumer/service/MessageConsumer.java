package mx.raul.activemq.queueproducerconsumer.service;

import mx.raul.activemq.queueproducerconsumer.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class MessageConsumer {

//  private JmsTemplate jmsTemplate;
//
//  @Autowired
//  public MessageConsumer(JmsTemplate jmsTemplate) {
//    this.jmsTemplate = jmsTemplate;
//  }

  @JmsListener(
      destination = "raul-messages-queue",
      containerFactory = "QueueListenerContainerFactory",
      subscription = "raul-messages-queue")
  public void onReceivedMessage(Message message) {
//    Message message = (Message) this.jmsTemplate.receiveAndConvert();
    System.out.println("RECEIVED: " + message.getMessage());
  }
}
